package testnodos;

public class DineroCuenta {
    private String nCuenta;
    private String dinero;

    public DineroCuenta(String nCuenta, String dinero) {
        this.nCuenta = nCuenta;
        this.dinero = dinero;
    }

    public String getnCuenta() {
        return nCuenta;
    }

    void setnCuenta(String nCuenta) {
        this.nCuenta = nCuenta;
    }

    public String getDinero() {
        return dinero;
    }

    void setDinero(String dinero) {
        this.dinero = dinero;
    }


}