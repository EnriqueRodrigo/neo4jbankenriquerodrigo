package testnodos;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.SessionConfig;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.TransactionWork;
import org.neo4j.driver.Ne4jKike.tasks.service.MainBank;

public class grafo {
	public static Driver driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic("Banker", "123"));
	public static void main(String [] args)
    {
    	for (Entry<String, String> entry : crearNodo().entrySet()) {
    		System.out.println("Nodo--->" + entry.getValue());
    		DineroCuenta d = nodos(entry.getKey());
    		System.out.println("-------Relaciones-------------");
    		Graph graph = new SingleGraph("Test");
    		graph.addNode(entry.getValue());
    		graph.addNode(d.getDinero());
    		graph.addNode(d.getnCuenta());
    		graph.addEdge(entry.getValue()+d.getDinero(), entry.getValue(), d.getDinero());
    		graph.addEdge(entry.getValue()+d.getnCuenta(), entry.getValue(), d.getnCuenta());
    	}
    }
	public static Map<String,String>  crearNodo() {
        try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
            return session.writeTransaction( new TransactionWork<Map<String,String>>() {
                 @Override
                 public Map<String,String> execute(Transaction tx) {

                     Result result = tx.run( "MATCH(n:Usuario) RETURN n.dni, n.nombre");

                     return resultToMap(result.list());
                 }
             } );
         } 
    }
	 private static Map<String,String> resultToMap(List<Record> list) {
	        return list
	                .parallelStream()
	                .collect(Collectors.toMap(
	                        elem -> elem.get(0).toString().replace(Character.toString('"'), ""), 
	                        elem -> elem.get(1).toString().replace(Character.toString('"'), "")
	                        ));
	    }
	 public static DineroCuenta nodos(String dni) {
	        try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
	            return session.writeTransaction( new TransactionWork<DineroCuenta>() {
	                 @Override
	                 public DineroCuenta execute(Transaction tx) {

	                     Result result2 = tx.run( "MATCH (x:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance {dni:'" + dni + "'}]->(y) RETURN y.cantidad");
	                     Result result1 = tx.run( "MATCH (x:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Cuenta {dni:'" + dni + "'}]->(y) RETURN y.numero");

	                     return new DineroCuenta(resultToString(result1.list()), resultToString(result2.list()));
	                 }

					private String resultToString(List<Record> list) {
						return list.get(0).get(0).toString().replace(Character.toString('"'), "");
					}
	             } );
	         } 
	 }
}
