package org.neo4j.driver.Ne4jKike.tasks.service;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.neo4j.driver.*;


import static org.neo4j.driver.Values.parameters;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;



public class MainBank extends JFrame
{
    // Driver objects are thread-safe and are typically made available application-wide.
    public static Driver driver;
    
    private static JPanel PanelLogIn;
    private static PanelUser PanelUser;
    private static PanelBanker PanelBanker;
    
    private static String dni="";
    public static String name="";
    public static String namebanker="";
    public static String lastname="";
    public static String lastnamebanker="";
    public static String accountnumber="";
    public static String balance="";

    private JTextField LogInIdTextField;
    private JTextField LogInPinTextField;
    
    public  void logout1() {
    	PanelLogIn.setVisible(true);
    	PanelUser.setVisible(false);
    	setContentPane(PanelLogIn);
    }
    
    public  void logout2() {
    	PanelLogIn.setVisible(true);
    	PanelBanker.setVisible(false);
    	setContentPane(PanelLogIn);
    }
    
    public static long random() {
        long n = 0;
        while (String.valueOf(n).length() != 12) {
            n = Math.abs(new Random().nextLong()) / 10000000;
        }
        return n;
    }
    
 
    	public static boolean sumardinero(int money) {//Chequear credenciales Usuario
            try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
               return session.writeTransaction( new TransactionWork<Boolean>() {
                    @Override
                    public Boolean execute(Transaction tx) {
                        int contador = 0;
                        Result result = tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) RETURN y.cantidad");
                        contador = Integer.valueOf(result.list().get(0).get(0).toString());
                        contador=contador+money;
                        tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) SET y.cantidad = " + contador + " RETURN y");           
                        return true;
                    }
                } );
            } catch (Exception e) {
                return false;
            }
        } 
    	
    	public static boolean restardinero(int money) {//Chequear credenciales Usuario
            try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
               return session.writeTransaction( new TransactionWork<Boolean>() {
                    @Override
                    public Boolean execute(Transaction tx) {
                        int contador = 0;
                        Result result = tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) RETURN y.cantidad");
                        contador = Integer.valueOf(result.list().get(0).get(0).toString());
                        contador=contador-money;
                        tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) SET y.cantidad = " + contador + " RETURN y");                    
                        return true;
                    }
                } );
            } catch (Exception e) {
                return false;
            }
        } 
    	
    	public static boolean tranferirdinero(int money,String dni2,String accountnumber) {//Chequear credenciales Usuario
            try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
               return session.writeTransaction( new TransactionWork<Boolean>() {
                    @Override
                    public Boolean execute(Transaction tx) {
                        int contador = 0;      
                        Result result = tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) RETURN y.cantidad");
                        contador = Integer.valueOf(result.list().get(0).get(0).toString());
                        contador=contador-money;
                        tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) SET y.cantidad = " + contador + " RETURN y");
                        Result result1 = tx.run( "MATCH (n:Usuario {dni:'" + dni2 + "'}) MATCH (x)-[:Balance{dni:'"+dni2+"'}]->(y) RETURN y.cantidad");
                        contador=Integer.valueOf(result1.list().get(0).get(0).toString());
                        contador=contador+money;
                        tx.run( "MATCH (n:Usuario {dni:'" + dni2 + "'}) MATCH (x)-[:Balance{dni:'"+dni2+"'}]->(y) SET y.cantidad = " + contador + " RETURN y");
                        return true;
                    }
                } );
            } catch (Exception e) {
                return false;
            }
        } 
    	
     	public static boolean crearcuenta(String name,String lastname, String dni2,String pin,String role) {//Chequear credenciales Usuario	
     		if(role=="User")
     		{
                try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
                    return session.writeTransaction( new TransactionWork<Boolean>() {
                         @Override
                         public Boolean execute(Transaction tx) {  
                         	long r;
                         	r=random();
                             Result result = tx.run( "CREATE (n:Usuario{nombre:'"+name+"',apellido:'"+lastname+"',dni:'"+dni2+"',pin:'"+pin+"',role:'User'})CREATE (s:nCuenta {numero:"+r+",dni:'"+dni2+"'})CREATE(x:Dinero{cantidad:0,dni:'"+dni2+"'})");
                             Result result1 = tx.run( "MATCH (n:Usuario {dni:'"+dni2+"'}) MATCH (x:nCuenta {numero:"+r+"}) CREATE (n)-[:Cuenta {dni:'"+dni2+"'}]->(x)");
                             Result result2 = tx.run( "MATCH (s:Usuario {dni:'"+dni2+"'}) MATCH (x:Dinero {dni:'"+dni2+"'}) CREATE (s)-[:Balance {dni:'"+dni2+"'}]->(x)");
                             return true;
                         }
                     } );
                 } catch (Exception e) {
                     return false;
                 }
     		}
     		else
     		{
                try ( Session session = driver.session(SessionConfig.forDatabase("bankers")) ) {
                    return session.writeTransaction( new TransactionWork<Boolean>() {
                         @Override
                         public Boolean execute(Transaction tx) {  
                         	long r;
                         	r=random();
                             Result result = tx.run( "CREATE (n:Bankero{nombre:'"+name+"',apellido:'"+lastname+"',dni:'"+dni2+"',pin:'"+pin+"',role:'Banker'})");                   
                             return true;
                         }
                     } );
                 } catch (Exception e) {
                     return false;
                 }
     		}
        } 
     	
     	
     	public static boolean borrarcuenta(String name,String lastname, String dni2,String pin,String role) {//Chequear credenciales Usuario	
     		if(role=="User")
     		{
                try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
                    return session.writeTransaction( new TransactionWork<Boolean>() {
                         @Override
                         public Boolean execute(Transaction tx) {  
                             Result result = tx.run( "MATCH (n:Usuario{nombre:'"+name+"',apellido:'"+lastname+"',dni:'"+dni2+"',pin:'"+pin+"',role:'User'}) DETACH DELETE n");
                             Result result1 = tx.run( "MATCH (s:nCuenta {dni:'"+dni2+"'}) DELETE s");
                             Result result2 = tx.run( "MATCH(x:Dinero{dni:'"+dni2+"'}) DELETE x");
                             return true;
                         }
                     } );
                 } catch (Exception e) {
                     return false;
                 }
     		}
     		else
     		{
                try ( Session session = driver.session(SessionConfig.forDatabase("bankers")) ) {
                    return session.writeTransaction( new TransactionWork<Boolean>() {
                         @Override
                         public Boolean execute(Transaction tx) {  
                         	long r;
                         	r=random();
                             Result result = tx.run( "MATCH (n:Bankero{nombre:'"+name+"',apellido:'"+lastname+"',dni:'"+dni2+"',pin:'"+pin+"',role:'Banker'}) DELETE n");                   
                             return true;
                         }
                     } );
                 } catch (Exception e) {
                     return false;
                 }
     		}
        } 
    
    public boolean credentialsIdentifierUser(String dni, String pin) {//Chequear credenciales Usuario
        try ( Session session = driver.session(SessionConfig.forDatabase("neo4j")) ) {
           return session.writeTransaction( new TransactionWork<Boolean>() {
                @Override
                public Boolean execute(Transaction tx) {
                    Result result = tx.run( "MATCH (n:Usuario {dni:'"+dni+"', pin:'"+pin+"'}) RETURN n.role");
                    List<Record> list = result.list();
                    if (list.isEmpty()) {
                        return false; 
                    } else {
                        String text = list.get(0).get(0).toString().replace(Character.toString('"'), "");
                        System.out.println(text);
                       // driver.close();
                        driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic(text, "123"));
                        result = tx.run( "MATCH (n:Usuario {dni:'"+dni+"', pin:'"+pin+"'}) RETURN n.nombre");
                        name = result.list().get(0).get(0).toString().replace(Character.toString('"'), "");
                        result = tx.run( "MATCH (n:Usuario {dni:'"+dni+"', pin:'"+pin+"'}) RETURN n.apellido");
                        lastname = result.list().get(0).get(0).toString().replace(Character.toString('"'), "");
                        result = tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Cuenta{dni:'"+dni+"'}]->(y) RETURN y.numero");
                        accountnumber=result.list().get(0).get(0).toString();
                        
                        result = tx.run( "MATCH (n:Usuario {dni:'" + dni + "'}) MATCH (x)-[:Balance{dni:'"+dni+"'}]->(y) RETURN y.cantidad");
                        balance=result.list().get(0).get(0).toString(); 
                    }
                    return true;
                }
            } );
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean credentialsIdentifierBanker(String dni, String pin) {//Chequear credenciales Banquero
        try ( Session session = driver.session(SessionConfig.forDatabase("bankers")) ) {
           return session.writeTransaction( new TransactionWork<Boolean>() {
                @Override
                public Boolean execute(Transaction tx) {
                    Result result = tx.run( "MATCH (n:Bankero {dni:'"+dni+"', pin:'"+pin+"'}) RETURN n.role");
                    List<Record> list = result.list();
                    if (list.isEmpty()) {
                        return false; 
                    } else {
                        String text = list.get(0).get(0).toString().replace(Character.toString('"'), "");
                        System.out.println(text);
                        driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic(text, "123"));
                        result = tx.run( "MATCH (n:Bankero {dni:'"+dni+"', pin:'"+pin+"'}) RETURN n.nombre");
                        namebanker = result.list().get(0).get(0).toString().replace(Character.toString('"'), "");
                        result = tx.run( "MATCH (n:Bankero {dni:'"+dni+"', pin:'"+pin+"'}) RETURN n.apellido");
                        lastnamebanker = result.list().get(0).get(0).toString().replace(Character.toString('"'), "");
                    }
                    return true;
                }
            } );
        } catch (Exception e) {
            return false;
        }
    }

    public MainBank(String uri, String user, String password)
    {
    	driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
    	
    	PanelUser = new PanelUser();
    	PanelUser.setVisible(false);
    	
    	PanelBanker = new PanelBanker();
    	PanelBanker.setVisible(false);
    	//Panel Usuario Invisible
    	

	    setTitle("NeoBank");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 345);
		PanelLogIn = new JPanel();
		PanelLogIn.setBorder(new EmptyBorder(6, 6, 6, 6));
		setContentPane(PanelLogIn);
		getContentPane().setLayout(null);
		PanelLogIn.setLayout(null);
		PanelLogIn.setVisible(true);
		
		JButton LogInButton = new JButton("Log In");
		LogInButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(credentialsIdentifierUser(LogInIdTextField.getText(),LogInPinTextField.getText()))
				{
					PanelLogIn.setVisible(false);
			    	PanelUser.setVisible(true);
					setContentPane(PanelUser);
					dni=LogInIdTextField.getText();
					org.neo4j.driver.Ne4jKike.tasks.service.PanelUser.userdata(name,lastname,accountnumber,balance);
				}
				else if(credentialsIdentifierBanker(LogInIdTextField.getText(),LogInPinTextField.getText()))
				{
					PanelLogIn.setVisible(false);
			    	PanelBanker.setVisible(true);
					setContentPane(PanelBanker);
					//dni=LogInIdTextField.getText();
					org.neo4j.driver.Ne4jKike.tasks.service.PanelBanker.bankerdata(namebanker,lastnamebanker);
				}
			}
		});
		LogInButton.setBounds(130, 205, 165, 23);
		PanelLogIn.add(LogInButton);
		
		LogInIdTextField = new JTextField();
		LogInIdTextField.setBounds(130, 86, 165, 20);
		PanelLogIn.add(LogInIdTextField);
		LogInIdTextField.setColumns(10);
		
		LogInPinTextField = new JTextField();
		LogInPinTextField.setBounds(130, 138, 165, 20);
		PanelLogIn.add(LogInPinTextField);
		LogInPinTextField.setColumns(10);
		
		JLabel LabelId = new JLabel("DNI");
		LabelId.setBounds(91, 89, 29, 14);
		PanelLogIn.add(LabelId);
		
		JLabel LabelPin = new JLabel("PIN");
		LabelPin.setBounds(91, 141, 29, 14);
		PanelLogIn.add(LabelPin);

		PanelUser.ButtonLogOutUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout1();
			}
		});
		
		PanelBanker.ButtonLogOutBanker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logout2();
			}
		});

		setVisible(true);
    }


    public void close()
    {
        // Closing a driver immediately shuts down all open connections.
        driver.close();
    }

    public static void main(String... args)
    {
        MainBank example = new MainBank("bolt://localhost:7687", "Banker", "123");
    }
}
