package org.neo4j.driver.Ne4jKike.tasks.service;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

import testnodos.DineroCuenta;
import testnodos.grafo;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;

public class PanelBanker extends JPanel {
	private JTextField TextName;
	private JTextField TextLastName;
	private JTextField TextID;
	private JTextField TextPin;
	static 	JButton ButtonLogOutBanker = new JButton("LogOut");
	static 	JButton ButtonCreateAccount = new JButton("CreateAccount");
	static 	JButton ButtonDeleteAccount = new JButton("DeleteAccount");
	
	private static JLabel LabelNameSet = new JLabel(MainBank.namebanker);
	private static JLabel LabelLastNameSet = new JLabel(MainBank.lastnamebanker);
	
	static JComboBox<String> AccountType = new JComboBox<String>();

	
	public static void bankerdata(String namebanker,String lastnamebanker)
	{
		LabelNameSet.setText(namebanker);
		LabelLastNameSet.setText(lastnamebanker);
	}
	/**
	 * Create the panel.
	 */
	public  Graph createGraph() {
		Graph graph = new SingleGraph("Usuarios");

	   	for (Entry<String, String> entry : grafo.crearNodo().entrySet()) {
    		DineroCuenta d = grafo.nodos(entry.getKey());
    		graph.addNode(entry.getKey()).setAttribute("ui.label", entry.getValue());
    		graph.addNode(entry.getKey()+d.getDinero()).setAttribute("ui.label", d.getDinero());;
    		graph.addNode(d.getnCuenta()).setAttribute("ui.label", d.getnCuenta());
    		graph.addEdge(entry.getKey()+d.getDinero(), entry.getKey(),entry.getKey()+ d.getDinero());
    		graph.addEdge(entry.getKey()+d.getnCuenta(), entry.getKey(), d.getnCuenta());
    		
    	}
	   	for(Node node : graph)
	   	{
	   		node.setAttribute("ui.style", "fill-color: #A6C3FC; text-alignment: at-right; text-size: 12;");
	   	}
		return graph;
	}
    
	public PanelBanker() {
		
		setLayout(null);
		
		JLabel LabelName = new JLabel("Nombre:");
		LabelName.setBounds(10, 36, 57, 14);
		add(LabelName);
		
		LabelNameSet.setBounds(77, 36, 98, 14);
		add(LabelNameSet);
		
		JLabel LabelLastName = new JLabel("Apellido :");
		LabelLastName.setBounds(10, 61, 57, 14);
		add(LabelLastName);
		
		JLabel label = new JLabel("New label");
		label.setBounds(87, 61, 7, 5);
		add(label);
		
		LabelLastNameSet.setBounds(77, 61, 98, 14);
		add(LabelLastNameSet);
		
		TextName = new JTextField();
		TextName.setBounds(269, 33, 131, 20);
		add(TextName);
		TextName.setColumns(10);
		
		TextLastName = new JTextField();
		TextLastName.setBounds(269, 58, 131, 20);
		add(TextLastName);
		TextLastName.setColumns(10);
		
		TextID = new JTextField();
		TextID.setBounds(269, 86, 131, 20);
		add(TextID);
		TextID.setColumns(10);
		
		TextPin = new JTextField();
		TextPin.setBounds(269, 117, 131, 20);
		add(TextPin);
		TextPin.setColumns(10);
		
		
		ButtonCreateAccount.setBounds(269, 148, 131, 23);
		add(ButtonCreateAccount);
		ButtonCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainBank.crearcuenta(TextName.getText(),TextLastName.getText(),TextID.getText(),TextPin.getText(),AccountType.getSelectedItem().toString());
			}
		});
		
		JLabel LabelName2 = new JLabel("Nombre");
		LabelName2.setBounds(213, 36, 46, 14);
		add(LabelName2);
		
		JLabel LabelLastName2 = new JLabel("Apellido");
		LabelLastName2.setBounds(213, 61, 46, 14);
		add(LabelLastName2);
		
		JLabel LabelID = new JLabel("DNI");
		LabelID.setBounds(213, 89, 46, 14);
		add(LabelID);
		
		JLabel LabelPin = new JLabel("PIN");
		LabelPin.setBounds(213, 120, 46, 14);
		add(LabelPin);
		
		
		AccountType.setBounds(172, 149, 87, 20);
		add(AccountType);
		AccountType.setModel(new DefaultComboBoxModel<String>(new String[] { "User","Banker"}));
		
		ButtonLogOutBanker.setBounds(10, 225, 143, 23);
		add(ButtonLogOutBanker);
		
		JButton ButtonGraph = new JButton("GraficoDeCuentas");
		ButtonGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Graph graph = createGraph();
		        System.setProperty("org.graphstream.ui", "swing");
		        graph.setStrict(false);
		        graph.setAutoCreate(true);

		        Viewer viewer = graph.display();
		        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);
			}
		});
		ButtonGraph.setBounds(251, 232, 149, 23);
		add(ButtonGraph);
		
		
		ButtonDeleteAccount.setBounds(269, 182, 131, 23);
		add(ButtonDeleteAccount);
		ButtonDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainBank.borrarcuenta(TextName.getText(),TextLastName.getText(),TextID.getText(),TextPin.getText(),AccountType.getSelectedItem().toString());
			}
		});

	}
}
