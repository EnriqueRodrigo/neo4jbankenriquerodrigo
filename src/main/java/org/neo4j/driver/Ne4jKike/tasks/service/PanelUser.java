package org.neo4j.driver.Ne4jKike.tasks.service;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import org.neo4j.driver.Session;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public  class PanelUser extends JPanel {
	private JTextField TextMoneyTake;
	private JTextField TextMoneyTransfer;
	private JTextField TextMoneyId;
	private JTextField TextAccountNumber;
	static 	JButton ButtonLogOutUser = new JButton("LogOut");
	static  JButton ButtonAddMoney = new JButton("Meter Dinero");
	static 	JButton ButtonTakeOutMoney = new JButton("SacarDinero");
	static 	JButton ButtonTransferMoney = new JButton("TransferirDinero");
	
	private static JLabel LabelNameSet = new JLabel(MainBank.name);
	private static JLabel LabelLastNameSet = new JLabel(MainBank.lastname);
	private static JLabel LabelAccountNumberSet = new JLabel(MainBank.accountnumber);
	private static JLabel LabelBalanceSet = new JLabel(MainBank.balance);
	/**
	 * Create the panel.
	 */
	public static void userdata(String name,String lastname,String accountnumber,String balance)
	{
		LabelNameSet.setText(name);
		LabelLastNameSet.setText(lastname);
		LabelAccountNumberSet.setText(accountnumber);
		LabelBalanceSet.setText(balance);
	}
	
	public PanelUser() {
		setLayout(null);
		
	
		
		
		ButtonTakeOutMoney.setBounds(279, 28, 143, 23);
		add(ButtonTakeOutMoney);
		ButtonTakeOutMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainBank.restardinero(Integer.valueOf(TextMoneyTake.getText()));
			}
		});
		
		TextMoneyTake = new JTextField();
		TextMoneyTake.setBounds(279, 62, 143, 20);
		add(TextMoneyTake);
		TextMoneyTake.setColumns(10);
		
		JLabel LabelAmountS = new JLabel("Cantidad");
		LabelAmountS.setBounds(214, 65, 61, 14);
		add(LabelAmountS);
		
		
		ButtonTransferMoney.setBounds(279, 130, 143, 23);
		add(ButtonTransferMoney);
		ButtonTransferMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainBank.tranferirdinero(Integer.valueOf(TextMoneyTransfer.getText()),TextMoneyId.getText(),TextAccountNumber.getText());
			}
		});
		
		TextMoneyTransfer = new JTextField();
		TextMoneyTransfer.setBounds(279, 164, 143, 20);
		add(TextMoneyTransfer);
		TextMoneyTransfer.setColumns(10);
		
		JLabel LabelAmountT = new JLabel("Cantidad");
		LabelAmountT.setBounds(214, 167, 61, 14);
		add(LabelAmountT);
		
		TextMoneyId = new JTextField();
		TextMoneyId.setBounds(279, 195, 143, 20);
		add(TextMoneyId);
		TextMoneyId.setColumns(10);
		
		JLabel LabelId = new JLabel("DNI");
		LabelId.setBounds(214, 198, 46, 14);
		add(LabelId);
		
		TextAccountNumber = new JTextField();
		TextAccountNumber.setBounds(279, 226, 143, 20);
		add(TextAccountNumber);
		TextAccountNumber.setColumns(10);
		
		JLabel LabelAccountNumber = new JLabel("NumerodeCuenta");
		LabelAccountNumber.setBounds(163, 229, 112, 14);
		add(LabelAccountNumber);
		
		JLabel LabelName = new JLabel("Nombre :");
		LabelName.setBounds(10, 32, 57, 14);
		add(LabelName);
		
		JLabel LabelAccountNumberMine = new JLabel("NumerodeCuenta :");
		LabelAccountNumberMine.setBounds(10, 97, 112, 14);
		add(LabelAccountNumberMine);
		
		
		LabelNameSet.setBounds(66, 32, 87, 14);
		add(LabelNameSet);
		
		
		LabelAccountNumberSet.setBounds(132, 97, 128, 14);
		add(LabelAccountNumberSet);


		
		ButtonLogOutUser.setBounds(10, 225, 143, 23);
		add(ButtonLogOutUser);
	
		

		ButtonAddMoney.setBounds(279, 93, 143, 23);
		add(ButtonAddMoney);
		ButtonAddMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainBank.sumardinero(Integer.valueOf(TextMoneyTake.getText()));
			}
		});
		
		JLabel LabelBalance = new JLabel("Balance :");
		LabelBalance.setBounds(10, 134, 57, 14);
		add(LabelBalance);
		
		
		LabelBalanceSet.setBounds(66, 134, 107, 14);
		add(LabelBalanceSet);
		
		JLabel LabelLastName = new JLabel("Apellido :");
		LabelLastName.setBounds(10, 65, 57, 14);
		add(LabelLastName);
		
		
		LabelLastNameSet.setBounds(66, 65, 87, 14);
		add(LabelLastNameSet);
		
		JLabel LabelSimbol = new JLabel("\u20AC");
		LabelSimbol.setBounds(183, 134, 46, 14);
		add(LabelSimbol);
		
	}
}
